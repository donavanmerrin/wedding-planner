const {Datastore} = require("@google-cloud/datastore")
const cors = require('cors')
const { application, query } = require("express")
const express = require('express')

const app = express()
app.use(express.json())
app.use(cors())

const datastore = new Datastore()

app.patch("/users/login", async(req, res)=>{
    const body = req.body
    const query = datastore.createQuery('Users').filter("password", "=", body.password).filter("email","=",body.email)
    const [data] = await datastore.runQuery(query)
    
    if(data[0].fname === undefined){
        res.status(404)
        res.send(`The username or password does not match, please check your credentials and check again`)
    }else{
        const result = {fname:data[0].fname,lname:data[0].lname}
        res.send(result)
    }
})
app.get("/users/:email/verify", async(req,res)=>{
    const key = datastore.key(['Users',req.params.email]) 
    const associate = await datastore.get(key)
    if(associate[0] === undefined){
        res.status(404)
        res.send(`The username or password does not match, please check your credentials and check again`)
    }else{
        res.status(200)
        res.send(true)
    }

})

const PORT = process.env.PORT || 3002
app.listen(PORT, ()=>console.log("App started"))