"use strict";
exports.__esModule = true;
exports.Expenses = exports.Wedding = void 0;
var Wedding = /** @class */ (function () {
    function Wedding(weddingId, weddingName, weddingDate, weddingLocation, weddingBudget) {
        this.weddingId = weddingId;
        this.weddingName = weddingName;
        this.weddingDate = weddingDate;
        this.weddingLocation = weddingLocation;
        this.weddingBudget = weddingBudget;
    }
    return Wedding;
}());
exports.Wedding = Wedding;
var Expenses = /** @class */ (function () {
    function Expenses(expensesId, reason, amount, weddingId) {
        this.expensesId = expensesId;
        this.reason = reason;
        this.amount = amount;
        this.weddingId = weddingId;
    }
    return Expenses;
}());
exports.Expenses = Expenses;
