"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var expenses_services_imple_1 = require("../services/expenses-services-imple");
var wedding_services_imple_1 = require("../services/wedding-services-imple");
var errors_1 = require("./errors");
var app = express_1["default"]();
app.use(express_1["default"].json());
var weddingServices = new wedding_services_imple_1.WeddingServiceImple;
var expensesServices = new expenses_services_imple_1.ExpensesServicesImple;
app.get("/weddings", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var result, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, weddingServices.getAllWeddings()];
            case 1:
                result = _a.sent();
                res.send(result);
                return [3 /*break*/, 3];
            case 2:
                error_1 = _a.sent();
                if (error_1 instanceof errors_1.MissingResourceError)
                    res.status(404);
                res.send(error_1);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/weddings/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var weddingId, wedding, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                weddingId = Number(req.params.id);
                return [4 /*yield*/, weddingServices.retrieveWeddingById(weddingId)];
            case 1:
                wedding = _a.sent();
                res.send(wedding);
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                if (error_2 instanceof errors_1.MissingResourceError)
                    res.status(404);
                res.send(error_2);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/weddings/:id/expenses", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        return [2 /*return*/];
    });
}); });
app.post("/weddings", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wedding = req.body;
                return [4 /*yield*/, weddingServices.createNewWedding(wedding)];
            case 1:
                result = _a.sent();
                res.status(201);
                res.send(result);
                return [2 /*return*/];
        }
    });
}); });
app["delete"]("/weddings/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var weddingId, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                weddingId = Number(req.params.id);
                return [4 /*yield*/, weddingServices.deleteWeddingById(weddingId)];
            case 1:
                _a.sent();
                res.status(205);
                res.send("The wedding with ID " + weddingId + " has been deleted");
                return [3 /*break*/, 3];
            case 2:
                error_3 = _a.sent();
                if (error_3 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_3);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.put("/weddings/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var weddingId, wedding, updatedWedding, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                weddingId = Number(req.params.id);
                wedding = req.body;
                return [4 /*yield*/, weddingServices.updateWeddingById(wedding)];
            case 1:
                updatedWedding = _a.sent();
                res.send(updatedWedding);
                return [3 /*break*/, 3];
            case 2:
                error_4 = _a.sent();
                if (error_4 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_4);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/expenses", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var result, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, expensesServices.retrieveAllExpenses()];
            case 1:
                result = _a.sent();
                res.send(result);
                return [3 /*break*/, 3];
            case 2:
                error_5 = _a.sent();
                if (error_5 instanceof errors_1.MissingResourceError)
                    res.status(404);
                res.send(error_5);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expensesId, expenses, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                expensesId = Number(req.params.id);
                return [4 /*yield*/, expensesServices.retrieveExpenseByID(expensesId)];
            case 1:
                expenses = _a.sent();
                res.send(expenses);
                return [3 /*break*/, 3];
            case 2:
                error_6 = _a.sent();
                if (error_6 instanceof errors_1.MissingResourceError)
                    res.status(404);
                res.send(error_6);
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.post("/expenses", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expenses, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                expenses = req.body;
                return [4 /*yield*/, expensesServices.createNewExpense(expenses)];
            case 1:
                result = _a.sent();
                res.status(201);
                res.send(result);
                return [2 /*return*/];
        }
    });
}); });
app.put("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expensesId, expenses, updatedExpense, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                expensesId = Number(req.params.id);
                expenses = req.body;
                return [4 /*yield*/, expensesServices.updateExpenseByTheId(expenses)];
            case 1:
                updatedExpense = _a.sent();
                res.send(updatedExpense);
                return [3 /*break*/, 3];
            case 2:
                error_7 = _a.sent();
                if (error_7 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_7);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app["delete"]("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expensesId, error_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                expensesId = Number(req.params.id);
                return [4 /*yield*/, expensesServices.deleteExpenseByTheId(expensesId)];
            case 1:
                _a.sent();
                res.status(205);
                res.send("The Expense with the ID " + expensesId + " has been deleted");
                return [3 /*break*/, 3];
            case 2:
                error_8 = _a.sent();
                if (error_8 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_8);
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.listen(3000, function () { console.log("Application Started"); });
