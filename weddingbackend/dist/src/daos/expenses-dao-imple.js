"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.ExpensesDaoPostgress = void 0;
var connection_1 = require("../connection");
var entitities_1 = require("../entitities");
var errors_1 = require("../errors");
var ExpensesDaoPostgress = /** @class */ (function () {
    function ExpensesDaoPostgress() {
    }
    ExpensesDaoPostgress.prototype.getAllExpenses = function () {
        return __awaiter(this, void 0, void 0, function () {
            var sql, result, expensess, _i, _a, row, expenses;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        sql = "select * from expenses";
                        return [4 /*yield*/, connection_1.client.query(sql)];
                    case 1:
                        result = _b.sent();
                        expensess = [];
                        for (_i = 0, _a = result.rows; _i < _a.length; _i++) {
                            row = _a[_i];
                            expenses = new entitities_1.Expenses(row.w_id, row.reason, row.amount, row.expensesId);
                            expensess.push(expenses);
                        }
                        return [2 /*return*/, expensess];
                }
            });
        });
    };
    ExpensesDaoPostgress.prototype.getExpenseById = function (expensesId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result, row, expenses;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'select * from expenses where expenses_id = $1';
                        values = [expensesId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The expense with " + expensesId + " does not exist");
                        }
                        row = result.rows[0];
                        expenses = new entitities_1.Expenses(row.w_id, row.reason, row.amount, row.expensesId);
                        return [2 /*return*/, expenses];
                }
            });
        });
    };
    ExpensesDaoPostgress.prototype.getWeddingExpensesById = function (weddingId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = "select * from expenses where wedding_id=$1";
                        values = [weddingId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        if (result.rowCount === 0) {
                            throw new errors_1.MissingResourceError("The expenses for wedding with the id " + weddingId + " does not exist");
                        }
                        return [2 /*return*/, result.rows];
                }
            });
        });
    };
    ExpensesDaoPostgress.prototype.createExpense = function (expenses, weddingId) {
        return __awaiter(this, void 0, void 0, function () {
            var sqltest, valtest, resttest, sql2, values2, result2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sqltest = "select wedding_id from wedding where wedding_id = $1";
                        valtest = [weddingId];
                        return [4 /*yield*/, connection_1.client.query(sqltest, valtest)];
                    case 1:
                        resttest = _a.sent();
                        sql2 = "insert into expenses(reason,amount) values ($1,$2) returning expenses_id,w_id";
                        values2 = [expenses.reason, expenses.amount];
                        return [4 /*yield*/, connection_1.client.query(sql2, values2)];
                    case 2:
                        result2 = _a.sent();
                        expenses.expensesId = result2.rows[0].account_id;
                        return [2 /*return*/, expenses];
                }
            });
        });
    };
    ExpensesDaoPostgress.prototype.updateExpenseById = function (expenses) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'update expenses set reason = $1, amount = $2 where w_id = $3';
                        values = [expenses.reason, expenses.amount, expenses.weddingId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, expenses];
                }
            });
        });
    };
    ExpensesDaoPostgress.prototype.deleteExpenseById = function (expensesId) {
        return __awaiter(this, void 0, void 0, function () {
            var sql, values, result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        sql = 'delete from expenses where expenses_id = $1';
                        values = [expensesId];
                        return [4 /*yield*/, connection_1.client.query(sql, values)];
                    case 1:
                        result = _a.sent();
                        return [2 /*return*/, true];
                }
            });
        });
    };
    return ExpensesDaoPostgress;
}());
exports.ExpensesDaoPostgress = ExpensesDaoPostgress;
