"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var connection_1 = require("../src/connection");
var wedding_dao_imple_1 = require("../src/daos/wedding-dao-imple");
var entitities_1 = require("../src/entitities");
var weddingDao = new wedding_dao_imple_1.WeddingDaoPostgress;
test("Get all weddings", function () { return __awaiter(void 0, void 0, void 0, function () {
    var wedding1, wedding2, wedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wedding1 = new entitities_1.Wedding(0, "Sandy", "10-02-2022", "CA", 10000);
                wedding2 = new entitities_1.Wedding(0, "Eyo", "02-22-2022", "CA", 10000);
                return [4 /*yield*/, weddingDao.createWedding(wedding1)];
            case 1:
                _a.sent();
                return [4 /*yield*/, weddingDao.createWedding(wedding2)];
            case 2:
                _a.sent();
                return [4 /*yield*/, weddingDao.getWeddings()];
            case 3:
                wedding = _a.sent();
                expect(wedding.length).toBeGreaterThanOrEqual(2);
                return [2 /*return*/];
        }
    });
}); });
test("Get Wedding By Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, getwedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wedding = new entitities_1.Wedding(0, "Jesters", "04-15-2023", "HI", 10000);
                return [4 /*yield*/, weddingDao.createWedding(wedding)];
            case 1:
                wedding = _a.sent();
                wedding.weddingId;
                return [4 /*yield*/, weddingDao.getWeddingById(wedding.weddingId)];
            case 2:
                getwedding = _a.sent();
                expect(getwedding.weddingName).toBe(wedding.weddingName);
                return [2 /*return*/];
        }
    });
}); });
test("Create new Wedding", function () { return __awaiter(void 0, void 0, void 0, function () {
    var testWedding, wedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                testWedding = new entitities_1.Wedding(0, "Jesters", "04-15-2023", "HI", 10000);
                return [4 /*yield*/, weddingDao.createWedding(testWedding)];
            case 1:
                wedding = _a.sent();
                expect(wedding).not.toBe(0);
                return [2 /*return*/];
        }
    });
}); });
test("Update Wedding By Id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var wedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wedding = new entitities_1.Wedding(2, "Merrin", "01-01-2027", "Peru", 10000);
                return [4 /*yield*/, weddingDao.createWedding(wedding)];
            case 1:
                wedding = _a.sent();
                wedding.weddingName = "Mora";
                return [4 /*yield*/, weddingDao.updateWedding(wedding)];
            case 2:
                wedding = _a.sent();
                expect(wedding.weddingName).toBe("Mora");
                return [2 /*return*/];
        }
    });
}); });
test("Delete wedding By ID", function () { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, wedding2, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wedding = new entitities_1.Wedding(0, "Ash", "02-14-2028", "CA", 10000);
                return [4 /*yield*/, weddingDao.createWedding(wedding)];
            case 1:
                wedding2 = _a.sent();
                return [4 /*yield*/, weddingDao.deleteWedding(wedding2.weddingId)];
            case 2:
                result = _a.sent();
                expect(result).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        connection_1.client.end;
        return [2 /*return*/];
    });
}); });
