import { client } from "../src/connection"
import { ExpensesDao } from "../src/daos/expenses-dao"
import { ExpensesDaoPostgress } from "../src/daos/expenses-dao-imple"
import { Expenses } from "../src/entitities"

const expensesDao:ExpensesDao = new ExpensesDaoPostgress

test("Get all expenses", async()=>{
    let expenses:Expenses = new Expenses(0,"Flowers",500,5)
    await expensesDao.createExpense(expenses,expenses.weddingId)

    const expensess:Expenses[] = await expensesDao.getAllExpenses()
    expect (expensess.length).toBeGreaterThanOrEqual(1)
})
test("Get all wedding expenses", async()=>{
    const testExpense1:Expenses = new Expenses(0,"Florist",1500,5)
    await expensesDao.createExpense(testExpense1,testExpense1.weddingId)

    const result = await expensesDao.getWeddingExpensesById(5)
    expect(result.length).toBeGreaterThanOrEqual(1)
})
test("Get expenses by Id", async()=>{
    let expense:Expenses = new Expenses(0,"food",1000,2)
    await expensesDao.createExpense(expense,expense.weddingId)
    const expenses:Expenses[] = await expensesDao.getAllExpenses()
    expect (expenses.length).toBeGreaterThanOrEqual(1)
})
test("Create new Expense", async()=>{
    let expense1:Expenses = new Expenses(0,"Gifts",1000,5)
    await expensesDao.createExpense(expense1,expense1.weddingId)
    const expense:Expenses[] = await expensesDao.getAllExpenses()
    expect (expense.length).toBeGreaterThanOrEqual(1)
})
test("Update expense by id", async()=>{
    let expense1:Expenses = new Expenses(0,"Gifts",1000,5)
    await expensesDao.createExpense(expense1,expense1.weddingId)
    expense1.reason = "More Gifts"
    const expenses:Expenses = await expensesDao.updateExpenseById(expense1)
    expect (expenses.reason).toBe("More Gifts")
})
test("Delete expense by id", async()=>{
    let expense:Expenses = new Expenses(0,"Rings",1000,5)
    let expense2:Expenses = await expensesDao.createExpense(expense,expense.expensesId)
    const result:boolean = await expensesDao.deleteExpenseById(expense2.expensesId)
    expect(result).toBeTruthy()
})
afterAll(async()=>{
    client.end
})