
export class Wedding{
    constructor(
        public weddingId: number,
        public weddingName: string,
        public weddingDate: string,
        public weddingLocation: string,
        public weddingBudget: number
    ){}
}

export class Expenses{
    constructor(
        public expensesId: number,
        public reason: string,
        public amount:number,
        public weddingId:number,
    ){}
}