import express from 'express'
import ExpensesServices from '../services/expenses-services'
import { ExpensesServicesImple } from '../services/expenses-services-imple'
import WeddingServices from '../services/wedding-services'
import { WeddingServiceImple } from '../services/wedding-services-imple'
import { Expenses,Wedding } from './entitities'
import { MissingResourceError } from './errors'


const app =express()
app.use(express.json())

const weddingServices:WeddingServices = new WeddingServiceImple
const expensesServices:ExpensesServices = new ExpensesServicesImple

app.get("/weddings",async (req,res)=>{
    try{
        const result:Wedding[] = await weddingServices.getAllWeddings()
        res.send(result)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
    }
})

app.get("/weddings/:id",async (req,res)=>{
    try{
        const weddingId:number = Number(req.params.id)
        const wedding:Wedding = await weddingServices.retrieveWeddingById(weddingId)
        res.send(wedding)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
    }
})
app.get("/weddings/:id/expenses",async (req,res)=>{
    try{
        const weddingId = Number(req.params.id)
        const expenses:Expenses[] = await expensesServices.retrieveWeddingExpensesById(weddingId)
        res.send(expenses)
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404)
            res.send(error)
        }
    }
})

app.post("/weddings",async (req,res)=>{
    const wedding:Wedding = req.body
    const result:Wedding = await weddingServices.createNewWedding(wedding)
    res.status(201)
    res.send(result)
})

app.delete("/weddings/:id",async (req,res)=>{
    try{
        const weddingId = Number(req.params.id)
        await weddingServices.deleteWeddingById(weddingId)
        res.status(205)
        res.send(`The wedding with ID ${weddingId} has been deleted`)

    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404)
            res.send(error)
        }
    }
})

app.put("/weddings/:id",async (req,res)=>{
    try{
        const weddingId:number = Number(req.params.id)
        const wedding:Wedding = req.body

        const updatedWedding = await weddingServices.updateWeddingById(wedding)
        res.send(updatedWedding)
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404)
            res.send(error)
        }
    }
})


app.get("/expenses",async (req,res)=>{
    try{
        const result:Expenses[] = await expensesServices.retrieveAllExpenses()
        res.send(result)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
    }
})

app.get("/expenses/:id",async (req,res)=>{
    try{
        const expensesId:number = Number(req.params.id)
        const expenses:Expenses = await expensesServices.retrieveExpenseByID(expensesId)
        res.send(expenses)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
    }
})

app.post("/expenses",async (req,res)=>{
    const expenses:Expenses = req.body
    const result:Expenses = await expensesServices.createNewExpense(expenses)
    res.status(201)
    res.send(result)
})

app.put("/expenses/:id",async (req,res)=>{
    try{
        const expensesId:number = Number(req.params.id)
        const expenses:Expenses = req.body

        const updatedExpense = await expensesServices.updateExpenseByTheId(expenses)
        res.send(updatedExpense)
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404)
            res.send(error)
        }
    }
})

app.delete("/expenses/:id",async (req,res)=>{
    try{
        const expensesId = Number(req.params.id)
        await expensesServices.deleteExpenseByTheId(expensesId)
        res.status(205)
        res.send(`The Expense with the ID ${expensesId} has been deleted`)
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404)
            res.send(error)
        }
    }
})

app.listen(3000,()=>{console.log("Application Started")})