import { Wedding } from "../entitities";

export interface WeddingDao{

    getWeddings():Promise<Wedding[]>;
    
    getWeddingById(weddingId:number):Promise<Wedding>

    createWedding(wedding:Wedding):Promise<Wedding>

    updateWedding(wedding:Wedding):Promise<Wedding>

    deleteWedding(weddingId:number):Promise<boolean>
}