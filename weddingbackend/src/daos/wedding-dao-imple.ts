import { client } from "../connection";
import { Wedding } from "../entitities";
import { MissingResourceError } from "../errors";
import { WeddingDao } from "./wedding-dao";


export class WeddingDaoPostgress implements WeddingDao{
    async getWeddings(): Promise<Wedding[]> {
        const sql:string = 'select * from wedding';
        const result = await client.query(sql);
        const weddings:Wedding[] = [];
        for(const row of result.rows){
            const wedding:Wedding = new Wedding(
                row.wedding_id,
                row.wedding_name,
                row.wedding_date,
                row.wedding_location,
                row.wedding_budget
                );
            weddings.push(wedding);
            }
            return weddings;
    }
    async getWeddingById(weddingId: number): Promise<Wedding> {
        const sql:string = 'select * from wedding where wedding_id = $1';
        const values = [weddingId]
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with ${weddingId} does not exist`)
        }
        const row = result.rows[0];
        const wedding:Wedding = new Wedding(
            row.wedding_id,
            row.wedding_name,
            row.wedding_date,
            row.wedding_location,
            row.wedding_budget
                );
            return wedding;  
          }
    async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "insert into wedding(wedding_name,wedding_date,wedding_location,wedding_budget) values ($1,$2) returning wedding_id"
        const values = [wedding.weddingName,wedding.weddingDate,wedding.weddingLocation,wedding.weddingBudget];
        const result = await client.query(sql,values);
        wedding.weddingId = result.rows[0].wedding_id;
            
        return wedding;
        }
    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = 'update wedding set wedding_name =$1, wedding_date = $2, wedding_location = $3, wedding_budget = $4 where wedding_id = $5';
        const values = [wedding.weddingName,wedding.weddingDate,wedding.weddingLocation,wedding.weddingBudget,wedding.weddingId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`the wedding with id ${wedding.weddingId} does not exist`)
        }
        return wedding; 
       }
    async deleteWedding(weddingId: number): Promise<boolean> {
        const sql1:string = 'delete from expenses where w_id=$1'
        const val2 = [weddingId]
        const result2 = await client.query(sql1,val2)

        const sql:string = 'delete from wedding where wedding_id=$1';
        const values = [weddingId];
        const result = await client.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`the wedding with id ${weddingId} does not exist`)
        }
        return true;
    }
    
}