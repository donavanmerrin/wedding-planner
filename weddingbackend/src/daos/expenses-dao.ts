import { Expenses } from "../entitities";


export interface ExpensesDao{

    getAllExpenses():Promise<Expenses[]>

    getExpenseById(expensesId:number):Promise<Expenses>

    getWeddingExpensesById(weddingId:number):Promise<Expenses[]>

    createExpense(expenses:Expenses,weddingId:number):Promise<Expenses>

    updateExpenseById(expenses:Expenses):Promise<Expenses>

    deleteExpenseById(expensesId:number):Promise<boolean>
}