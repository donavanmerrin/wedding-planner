import { WeddingDao } from "../src/daos/wedding-dao";
import { Wedding } from "../src/entitities";
import WeddingServices from "./wedding-services";
import { WeddingDaoPostgress } from "../src/daos/wedding-dao-imple";
import { MissingResourceError } from "../src/errors";

export class WeddingServiceImple implements WeddingServices{
    weddingDao:WeddingDao = new WeddingDaoPostgress()
    getAllWeddings(): Promise<Wedding[]> {
        return this.weddingDao.getWeddings()
        }
    retrieveWeddingById(weddingId: number): Promise<Wedding> {
        return this.weddingDao.getWeddingById(weddingId)
    }
    createNewWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDao.createWedding(wedding)
    }
    async updateWeddingById(wedding: Wedding): Promise<Wedding> {
        const testWeddingId:Wedding = await this.weddingDao.getWeddingById(wedding.weddingId)
        if(testWeddingId.weddingId != wedding.weddingId){
            throw Error(`Wedding ID's do not match`)
        }
        return this.weddingDao.updateWedding(wedding)
    }
    async deleteWeddingById(weddingId: number): Promise<boolean> {
        const wedding:Wedding = await this.weddingDao.getWeddingById(weddingId)
        if(wedding.weddingId != weddingId){
            throw new MissingResourceError(`There is no wedding with the ID of ${weddingId}`)
        }
    return this.weddingDao.deleteWedding(weddingId)
    }
}