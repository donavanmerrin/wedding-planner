import { ExpensesDao } from "../src/daos/expenses-dao";
import { ExpensesDaoPostgress } from "../src/daos/expenses-dao-imple";
import { Expenses, Wedding } from "../src/entitities";
import { MissingResourceError } from "../src/errors";
import ExpensesServices from "./expenses-services";
import WeddingServices from "./wedding-services";


export class ExpensesServicesImple implements ExpensesServices{
    expensesDao:ExpensesDao = new ExpensesDaoPostgress

    retrieveAllExpenses(): Promise<Expenses[]> {
        return this.expensesDao.getAllExpenses()
    }
    retrieveExpenseByID(expensesId: number): Promise<Expenses> {
        return this.expensesDao.getExpenseById(expensesId)
    }
    retrieveWeddingExpensesById(weddingId: number): Promise<Expenses[]> {
        return this.expensesDao.getWeddingExpensesById(weddingId)
    }
    createNewExpense(expenses: Expenses): Promise<Expenses> {
    return this.expensesDao.createExpense(expenses,expenses.weddingId)
    }
    async updateExpenseByTheId(expenses: Expenses): Promise<Expenses> {
       return this.expensesDao.updateExpenseById(expenses)
    }
    async deleteExpenseByTheId(expensesId: number): Promise<boolean> {
        const expenses:Expenses = await this.expensesDao.getExpenseById(expensesId)
        if(expensesId != expenses.expensesId){
            throw new MissingResourceError(`There is no expense with the ID of ${expensesId}`)
        }
        return this.expensesDao.deleteExpenseById(expensesId)
    }
}