import { Expenses } from "../src/entitities";


export default interface ExpensesServices{

    retrieveAllExpenses():Promise<Expenses[]>

    retrieveExpenseByID(expensesId:number):Promise<Expenses>

    retrieveWeddingExpensesById(weddingId:number):Promise<Expenses[]>

    createNewExpense(expenses:Expenses):Promise<Expenses>

    updateExpenseByTheId(expenses:Expenses):Promise<Expenses>

    deleteExpenseByTheId(expensesId:number):Promise<boolean>
}