const {Datastore} = require("@google-cloud/datastore")
const datastore = new Datastore()
const cors = require('cors')
const express = require('express')
const axios = require('axios')
const date = new Date() 

const app = express()
app.use(cors())
app.use(express.json())

app.get("/messages", async (req,res)=>{
    const sender = req.query.sender
    const receiver = req.query.receiver

    if(sender !== undefined && receiver !== undefined){
        const query = datastore.createQuery('Messaging').filter("sender", "=", sender).filter("receiver","=",receiver)
        const [data,metaInfo] = await datastore.runQuery(query)
        res.send(data)
    } else if (sender !== undefined && receiver === undefined){
        const query = datastore.createQuery('Messaging').filter("sender", "=", sender)
        const [data,metaInfo] = await datastore.runQuery(query)
        res.send(data)
    } else if (sender === undefined && receiver !== undefined){
        const query = datastore.createQuery('Messaging').filter("receiver", "=", receiver)
        const [data,metaInfo] = await datastore.runQuery(query)
        res.send(data)
    } else {
        const query = datastore.createQuery('Messaging')
        const [data,metaData] = await datastore.runQuery(query)
        res.send(data)
    }

})


app.get("/messages/:mid",async (req,res)=>{
    try{const key = datastore.key(['Messaging', Number(req.params.mid)])
    const response = await datastore.get(key)
    res.send(response[0])
    }catch(error){
        res.send("No message with that ID, try again with a correct ID")
    }
})


app.post("/messages", async (req,res)=>{ 
    const body = req.body
    console.log(`https://authorization-service-dot-project1-324720.wm.r.appspot.com/users/${body.sender}/verify`)
    const employee1 = await axios.get(`https://authorization-service-dot-project1-324720.wm.r.appspot.com/users/${body.sender}/verify`)
    const employee2 = await axios.get(`https://authorization-service-dot-project1-324720.wm.r.appspot.com/users/${body.receiver}/verify`)
    if(employee1.data === employee2.data){

    const key = datastore.key('Messaging')

    const newMessage = {
        message: body.message,
        sender: body.sender,
        receiver: body.receiver,
        timestamp: date.toISOString()
    }
    const response3 = await datastore.save({key:key,data:newMessage});
    console.log(response3)
}
})

const PORT = process.env.PORT || 3000
app.listen(PORT,() =>console.log('Application Started :)'))