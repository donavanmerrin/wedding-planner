import React from 'react';
import ReactDOM from 'react-dom';
import GetExpenseById from './components/expenses-by-id';
import CreateExpense from './components/expenses-create';
import ExpensesDeletion from './components/expenses-delete';
import ExpensesPut from './components/expenses-update';
import GetExpenses from './components/get-expenses';
import GetWedding from './components/get-wedding';
import GetWeddingById from './components/get-wedding-by-id';
import GetWeddingExpensesById from './components/get-wedding-expenses';
import UploadFiles from './components/uploadfilecomponent/image-upload';
import LoginPage from './components/logincomponents/login';
import WeddingForm from './components/wedding-add';
import WeddingDeletion from './components/wedding-delete';
import WeddingPut from './components/wedding-put';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom"
import Messaging from './components/messagingcomponent/messaging';
import './styles.css'
import UploadImage from './components/uploadfilecomponent/image-upload2';
ReactDOM.render(
 
    <Router>
    <Switch>
    <Route exact path="/">
    <Redirect to="/login"></Redirect>
    </Route>
    <Route path="/login">  
    <LoginPage></LoginPage>
    </Route>
    <Route path="/weddings">
    <UploadImage></UploadImage>
    <GetWedding></GetWedding>
    <GetWeddingById></GetWeddingById>
    <GetWeddingExpensesById></GetWeddingExpensesById>
    <WeddingForm></WeddingForm>
    <WeddingPut></WeddingPut>
    <WeddingDeletion></WeddingDeletion>
    <GetExpenses></GetExpenses>
    <GetExpenseById></GetExpenseById>
    <CreateExpense></CreateExpense>
    <ExpensesPut></ExpensesPut>
    <ExpensesDeletion></ExpensesDeletion>
    </Route>
    <Route path='/messages'>
    <Messaging></Messaging>
    </Route> 
    </Switch>
    </Router>,
  document.getElementById('root')
);