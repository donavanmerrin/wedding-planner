import axios from 'axios';
import { useState,useRef } from 'react';
import ExpensesTable from './expenses-table';

export default function GetExpenseById(){
    const expenseIdInput = useRef(null);
    
    const [expenses,setExpenses] = useState([])
    async function getExpenseById(event){
        try{
        const id = expenseIdInput.current.value
        const array = []
        const response = await axios.get(`http://34.125.157.116:3000/expenses/${id}`)
        array.push(response.data)
        setExpenses(array)
        }catch(error){
            alert(`No expense with that ID`)
        }
    }

        return(<div>
            <h3>Get Expense By ID</h3>
            <input placeholder="Expense Id" ref={expenseIdInput}></input>
            <button onClick={getExpenseById}>Get Expense By Id</button>
            <ExpensesTable expenses={expenses}></ExpensesTable>
        </div>)
}