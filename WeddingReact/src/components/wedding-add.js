import axios from "axios"
import { useRef } from "react"

export default function WeddingForm(){
    
    const weddingNameInput = useRef(null);
    const weddingDateInput = useRef(null);
    const weddingLocationInput = useRef(null);
    const weddingBudgetInput = useRef(null);

    async function addWedding(){
        const wedding = {
            weddingId:0,
            weddingName:weddingNameInput.current.value,
            weddingDate:weddingDateInput.current.value,
            weddingLocation:weddingLocationInput.current.value,
            weddingBudget:Number(weddingBudgetInput.current.value)
        }
        
        const response = await axios.post('http://34.125.157.116:3000/weddings',wedding)
        alert("You created a wedding :)")
    }

    return (<div>
        <h3>Here You Can Create A New Wedding</h3>
        <input placeholder="Wedding Name" ref={weddingNameInput}></input>
        <input placeholder="Wedding Date" ref={weddingDateInput}></input>
        <input placeholder="Wedding Location" ref={weddingLocationInput}></input>
        <input placeholder="Wedding Budget" ref={weddingBudgetInput}></input>
        <button onClick={addWedding}>Add Wedding</button>
    </div>)
}