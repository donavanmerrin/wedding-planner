import ShowImage from "./image-show";
import axios from "axios"
import { useState } from 'react'


const base64js = require("base64-js");
export default function UploadFiles(){
    let readFile;

    const [name, setName] = useState()
    const [extension, setExtenstion] = useState()
    const [base64, setBase64] = useState()
    const [link, setLink] = useState()

    function readTheFile(){
        const content = readFile.result;
        const base = base64js.fromByteArray(content)
        console.log(base)
        console.log(content)
        setBase64(base)
    }
    async function uploadTheFile(){
        const uploadFile = {
            name: name,
            extension:extension,
            content: base64,
        }
        const response = await axios.post("https://us-west3-project1-324720.cloudfunctions.net/upload",uploadFile)
        console.log(uploadFile)
        setLink(response.data.photoLink)
        alert("You have uploaded a file")
    }
    function chooseFile(file){
        if(file.size <= 8000000){
            const[fname,fileExt]= file.name.split(".")
            setName(fname)
            setExtenstion(fileExt)
            readFile= new FileReader()
            readFile.onloadend = readTheFile;
            readFile.readAsArrayBuffer(file);
        } else{
            const size = Math.round(file.size / 1000000)
            alert(`File too big. The max size for an image is 10MB, the image you tried to upload is ${size}MB`)
        }
    }

        return(<div>
            <h1>Here you can upload an image</h1>
            <input type="file" id="file" accepts="image/*" onChange={(e)=> chooseFile(e.target.files[0])}></input>
            <button onClick={uploadTheFile}>Upload</button>
            <br></br>
            <ShowImage imageSrc={link}></ShowImage>
        </div>
        )
}