import axios from "axios"
import { useState } from "react"
import ShowImage from "./image-show"
const { encode, decode } = require("base64-arraybuffer")

export default function UploadImage(){
    let filereader;
    const URL = "https://us-west3-project1-324720.cloudfunctions.net/upload"
    const [name,setName] = useState()
    const [base64,setBase64] = useState()
    const [extension,setExtension] = useState()
    const [linkage,setLinkage] = useState()
    
    function readFile(){
        const content = filereader.result
        const base = encode(content)
        setBase64(base)
    }
    async function uploadTheFile(){
        const upFile={
            name:name,
            extension: extension,
            content: base64
        }
        const response = await axios.post(`${URL}`,upFile)
        setLinkage(response.data.photoLink);
        alert("You have uploaded a file")
    }
    function chooseFile(file){
        if(file.size <= 8000000){
            const[fname,fileExt]= file.name.split(".")
            setName(fname)
            setExtension(fileExt)
            filereader= new FileReader()
            filereader.onloadend = readFile;
            filereader.readAsArrayBuffer(file);
        } else{
            const size = Math.round(file.size / 1000000)
            alert(`File too big. The max size for an image is 10MB, the image you tried to upload is ${size}MB`)
        }
    }

    return(<div>
        <h1>Here you can upload an image</h1>
        <input type="file" id="file" accepts="image/*" onChange={(e)=> chooseFile(e.target.files[0])}></input>
        <button onClick={uploadTheFile}>Upload</button>
        <br></br>
    </div>
    )
}