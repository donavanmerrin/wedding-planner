import axios from "axios"
import { useRef } from "react"

export default function WeddingPut(){

    const weddingIdInput = useRef(null);
    const weddingNameInput = useRef(null);
    const weddingDateInput = useRef(null);
    const weddingLocationInput = useRef(null);
    const weddingBudgetInput = useRef(null);

    async function putWedding(){
        const wedding = {
            weddingId:weddingIdInput.current.value,
            weddingName:weddingNameInput.current.value,
            weddingDate:weddingDateInput.current.value,
            weddingLocation:weddingLocationInput.current.value,
            weddingBudget:Number(weddingBudgetInput.current.value)
        }
        try{
        const response = await axios.put(`http://34.125.157.116:3000/weddings/${weddingIdInput.current.value}`,wedding)
        alert("You updated a wedding :)")
        console.log(response)
    }catch(error){
        alert("No wedding with that ID")
    }
}

    return(<div>
        <h3>Here You Can Update A Wedding</h3>
        <input placeholder="Wedding Id" ref={weddingIdInput}></input>
        <input placeholder="Wedding Name" ref={weddingNameInput}></input>
        <input placeholder="Wedding Date" ref={weddingDateInput}></input>
        <input placeholder="Wedding Location" ref={weddingLocationInput}></input>
        <input placeholder="Wedding Budget" ref={weddingBudgetInput}></input>
        <button onClick={putWedding}>Update Wedding</button>
    </div>)
}