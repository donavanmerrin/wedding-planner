import axios from 'axios';
import { useState,useRef } from 'react';
import ExpensesTable from './expenses-table';

export default function CreateExpense(){
    
    const expensesReasonInput = useRef();
    const expenseAmountInput = useRef();
    const expenseWeddingIdInput = useRef();

    async function addExpense(){
        const expense = {
            expensesId:0,
            reason:expensesReasonInput.current.value,
            amount:expenseAmountInput.current.value,
            weddingId:Number(expenseWeddingIdInput.current.value)
        }
       
        const response = await axios.post('http://34.125.157.116:3000/expenses',expense)
        alert("You created an expense :)")
    
}
        return(<div>
            <h3>Create Expense</h3>
            <input placeholder="Expense Reason" ref={expensesReasonInput}></input>
            <input placeholder="Expense Amount" ref={expenseAmountInput}></input>
            <input placeholder="Wedding Id" ref={expenseWeddingIdInput}></input>
            <button onClick={addExpense}>Create Expense</button>
        </div>)
}