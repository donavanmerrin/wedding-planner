import axios from 'axios';
import { useState,useRef } from 'react';
import ExpensesTable from './expenses-table';
import WeddingTable from './wedding-table';



export default function GetWeddingExpensesById(){
    const weddingIdInput = useRef(null);
    
    const [expenses, setExpenses] = useState([])
    async function getWeddingExpensesById(event){
        try{ const id = weddingIdInput.current.value
        const response = await axios.get(`http://34.125.157.116:3000/weddings/${id}/expenses`)
        // array.push(response.data)
        setExpenses(response.data)
    }catch(error){
        alert("No wedding with that ID")
    }
    }
        return(<div>
            <h3>Get Wedding Expenses By Wedding Id</h3>
            <input placeholder="Wedding ID" ref={weddingIdInput}></input>
            <button onClick={getWeddingExpensesById}>Get Wedding Expenses By Wedding Id</button>
            <ExpensesTable expenses={expenses}></ExpensesTable>
        </div>)
}