import {useRef, useState} from "react"
import axios from "axios"

export default function LoginPage(){
    const loginEmail = useRef()
    const loginPassword = useRef()
    const [log, setLog] = useState()

    async function login(){
        const loginCredentials = { email: loginEmail.current.value, password: loginPassword.current.value }
        const response = await axios.patch(`https://project1-324720.wm.r.appspot.com/users/login`, loginCredentials)
        console.log(response)
        setLog(response.data.fname,response.data.lname)
        console.log(setLog)
        alert("You have succesfully logged in")
    }

    return(
    <>
        <h1>Welcome to the login page</h1>
        <h3>Please use your credentials to login</h3>
        <input 
        placeholder="Enter Email" 
        ref={loginEmail}>
        </input>
        
        <input
        placeholder="Enter Password" 
        type="password" 
        ref={loginPassword}>
    
        </input>
        <button onClick={login}>Login</button>
        {log === undefined ? (
            <div></div>
        ) : (
            <div>
                <p>Where would you like to go?</p>
                <a href="http://localhost:3000/weddings">
                    <button>Wedding/Expenses Services</button>
                </a>
                <a href="http://localhost:3000/messages">
                    <button>Messaging Page</button>
                </a>
            </div>
        )}
    </>)
}