import axios from "axios"
import {useRef} from "react"
import { useState } from "react"
import MessagingTable from "./messagingtable"

export default function Messaging(){

    const [message,setMessage] = useState([])
    async function getMessages(event){
        const response = await axios.get('https://messaging-service-dot-project1-324720.wm.r.appspot.com/messages')
        console.log(response.data)
        setMessage(response.data)
    }

    const senderNameInput = useRef() 
    const receiverNameInput = useRef()
    const messageInput = useRef()
    const senderName = useRef()
    const receiverName = useRef()
    
    async function addMessage(){
     
        const message = {
            receiver:receiverNameInput.current.value,
            sender:senderNameInput.current.value,
            message:messageInput.current.value,
        }
        const response = await axios.post('https://messaging-service-dot-project1-324720.wm.r.appspot.com/messages',message)
        alert("You have sent the message!")
    }

    const messageIdInput = useRef()
    const [messageDetails,setMessageDetails] = useState([])
    async function getMessageById(){
        try{
            const id = messageIdInput.current.value
            const array = []
            const response = await axios.get(`https://messaging-service-dot-project1-324720.wm.r.appspot.com/messages/${id}`)
        array.push(response.data)
        setMessage(array)
        
        }catch(error){
            alert(`No message with that id`)
        }
    }
        async function getMessagesByReceipient(){
        try{
            const id = receiverName.current.value
            //const array = []
            const response = await axios.get(`https://messaging-service-dot-project1-324720.wm.r.appspot.com/messages?receiver=${id}`)
            //array.push(response.data)
            setMessage(response.data)
        }catch(error){
        alert("No receipient with that Id")
    }
    }

    async function getMessagesBySender(){
        try{
            const identifier = senderName.current.value
            const response = await axios.get(`https://messaging-service-dot-project1-324720.wm.r.appspot.com/messages?sender=${identifier}`)
            setMessage(response.data)
        }catch(error){
            alert("No sender with that Id")
        }
    }

    async function getMessagesBySenderAndReceipient(){
        try{
            const id = senderName.current.value
            const id2 = receiverName.current.value
            const response = await axios.get(`https://messaging-service-dot-project1-324720.wm.r.appspot.com/messages?sender=${id}&receiver=${id2}`)
            setMessage(response.data)
        }catch(error){
            alert("No Messages between these two")
        }
    }
    

    return(<div>
        <h1>Here Is The Messaging Service</h1>
        <h3>Messaging</h3>
        <input placeholder="Message Id" ref={messageIdInput}></input>
        <input placeholder="Receipient Id" ref={receiverName}></input>
        <input placeholder="Sender Id" ref={senderName}></input>
        <button onClick={getMessages}>Get All Messages</button>
        <button onClick={getMessageById}>Get Message By Id</button>
        <button onClick={getMessagesByReceipient}>Get Messages By Receipient</button>
        <button onClick={getMessagesBySender}>Get Messages By Sender</button>
        <button onClick={getMessagesBySenderAndReceipient}>Get Messages Between People</button>
        <MessagingTable messages={message}></MessagingTable>
        <input placeholder="Sender Name" ref={senderNameInput}></input>
        <input placeholder="Receiver Name" ref={receiverNameInput}></input>
        <input placeholder="Message Details" ref={messageInput}></input>
        <button onClick={addMessage}>Send a Message</button>
    </div>)
}