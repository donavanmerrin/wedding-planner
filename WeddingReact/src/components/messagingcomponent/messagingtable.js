export default function MessagingTable(props){
    
    const messages = props.messages

    return(<table>
        <thead><tr>
            <th>Receiver</th>
            <th>Sender</th>
            <th>Message</th>
            <th>Timestamp</th>
        </tr></thead>
        <tbody>
            {messages.map(m =><tr>
                <td>{m.receiver}</td>
                <td>{m.sender}</td>
                <td>{m.message}</td>
                <td>{m.timestamp}</td>
            </tr>)}
        </tbody>
    </table>)
}
