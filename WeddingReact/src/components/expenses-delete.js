import axios from "axios"
import { useRef } from "react"

export default function ExpensesDeletion(){

    const expensesIdInput = useRef()

    async function deleteExpense(){
        try{
        const id = expensesIdInput.current.value
        const response = await axios.delete(`http://34.125.157.116:3000/expenses/${id}`)
        alert("You deleted an expense :)")
    }catch(error){
        alert("No expense with that Id")
    }
    }
    return(<div>
        <h3>Here You Can Delete An Expense</h3>
        <input placeholder="Expense ID" ref={expensesIdInput}></input>
        <button onClick={deleteExpense}>Delete Expense By Id</button>
        <br></br>
        <br></br>
        <a href="http://localhost:3000/messages">
                    <button>Messaging Page</button>
                </a>
    </div>)

}